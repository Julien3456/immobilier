package org.example;

import org.apache.spark.sql.Dataset;
import org.apache.spark.sql.Row;
import org.apache.spark.sql.SaveMode;

import java.util.Properties;

public class Database {
    private final Properties properties;
    private final String jdbcUrl;

    public Database(Properties properties) {
        this.properties = properties;
        String host = "localhost";
        String port = "5432";
        String database = "immobilier";
        this.jdbcUrl = "jdbc:postgresql://" + host + ":" + port + "/" + database;
    }

    public static Database createConnection(){
        String jdbcUrl = "jdbc:postgresql://localhost:5432/";
        String user = "moi";
        String password = "test";
        String jdbcDriver = "org.postgresql.Driver";

        Properties propConnection = new Properties();
        propConnection.put("user", user);
        propConnection.put("password", password);
        propConnection.put("jdbcUrl", jdbcUrl);
        propConnection.put("jdbcDriver", jdbcDriver);
        propConnection.put("dbname", "immobilier");

        return new Database(propConnection);
    }

    public void saveDataset(String table, Dataset<Row> dataset) {
        dataset.write()
                .mode(SaveMode.Overwrite)
                .jdbc(this.jdbcUrl, table, this.properties);
    }
}
