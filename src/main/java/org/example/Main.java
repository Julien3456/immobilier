package org.example;

import org.apache.spark.internal.config.R;
import org.apache.spark.sql.*;

import java.util.Arrays;
import java.util.List;

import static org.apache.spark.sql.functions.*;
import static org.example.Database.createConnection;

public class Main {

    private static Dataset<Row> createDataset(
            SparkSession session,
            String delimiter,
            String file) {
        return session
                .read()
                .format("csv")
                .option("header", "true")
                .option("delimiter", delimiter)
                .load("/home/julien/Documents/epsi/Cours/data_visu/workspace/src/main/resources/" + file);
    }

    private static Dataset<Row> filterByDep(
            Dataset<Row> dataset,
            String[] filter,
            String nameCol) {
        return dataset
                .filter(
                        dataset.col(nameCol)
                                .isInCollection(
                                        Arrays.asList(filter)
                                ));
    }

    public static void main(String[] args) {

        // Connect to database
        Database database = createConnection();
        String joinColumnRental = "DEP";
        String[] listDep = new String[]{"09", "11", "12", "30", "31", "32", "34", "46", "48", "65", "66", "81", "82"};        // Create session
        SparkSession sparkSession = SparkSession
                .builder().appName("immo")
                .master("local")
                .getOrCreate();
//        JavaSparkContext sparkContext = JavaSparkContext.fromSparkContext(sparkSession.sparkContext());

        // Collect ImmoSale's Datas
        Dataset<Row> dataSale = createDataset(sparkSession, ",", "full.csv");

        // Collect ImmoRental's Datas
        Dataset<Row> dataRentalHouse = createDataset(sparkSession, ";", "pred-mai-mef-dhup.csv");

        // Collect ImmoRental's Datas
        Dataset<Row> dataRentalApp = createDataset(sparkSession, ";", "pred-app-mef-dhup.csv");

        // Collect tax households
        Dataset<Row> dataHouseHolds = createDataset(sparkSession, ";", "FILO2021_DEC_COM.csv");

        // Collect tax households
        Dataset<Row> dataVacant = createDataset(sparkSession, ",", "logements-vacants-2021.csv");

        // Collect tax households
        Dataset<Row> dataCriminalite = createDataset(sparkSession, ";", "donnee-criminalite-2023.csv");

        dataCriminalite.show();

        // Filtering with house sold in Occitanie and group By "nom_commune"
        Dataset<Row> dataset_filtredDep = filterByDep(dataSale, listDep, "code_departement");
        Dataset<Row> dataset_filtredDep_clean = dataset_filtredDep
                .filter(
                        dataset_filtredDep.col("valeur_fonciere").isNotNull()
                                .and(dataset_filtredDep.col("surface_reelle_bati").isNotNull())
                                .and(dataset_filtredDep.col("type_local").equalTo("Maison"))
                                .or(dataset_filtredDep.col("type_local").equalTo("Appartement")));


        Dataset<Row> datasetFindRatio = dataset_filtredDep_clean.withColumn(
                "ratio", col("valeur_fonciere").divide(col("surface_reelle_bati")));

        // Filtering with house sold in Occitanie and group By "nom_commune"
        Dataset<Row> dataset_avgCommune = datasetFindRatio
                .groupBy(datasetFindRatio.col("nom_commune"), datasetFindRatio.col("type_local"))
                .agg(round(avg(datasetFindRatio.col("ratio")), 2).alias("tarif_m2"));

        // Filtering with house sold in Occitanie and group By "code_departement"
        Dataset<Row> dataset_avgDep = datasetFindRatio
                .groupBy(datasetFindRatio.col("code_departement"), datasetFindRatio.col("type_local"))
                .agg(round(avg(datasetFindRatio.col("ratio")), 2).alias("tarif_m2"));

        database.saveDataset("avg_vente_commune", dataset_avgCommune);
        database.saveDataset("avg_vente_departement", dataset_avgDep);

// PARTIE LOCATIF
        Dataset<Row> dataset_rental_house = filterByDep(dataRentalHouse, listDep, "DEP");

// HOUSE
        Dataset<Row> dataset_rental_house_cast = dataset_rental_house.withColumn("loypredm2",
                functions.regexp_replace(dataset_rental_house.col("loypredm2"), ",", "."));
        dataset_rental_house_cast.col("loypredm2").cast("float");
//        dataset_rental_house_cast.show();


        Dataset<Row> dataset_avg_rental_house_commune = dataset_rental_house_cast
                .groupBy(dataset_rental_house_cast.col("LIBGEO").alias("commune"), dataset_rental_house_cast.col("DEP"))
                .agg(round(avg(dataset_rental_house_cast.col("loypredm2")), 2).alias("avg_loyer_house"));

        Dataset<Row> dataset_avg_rental_house_dep = dataset_rental_house_cast
                .groupBy(dataset_rental_house_cast.col("DEP"))
                .agg(round(avg(dataset_rental_house_cast.col("loypredm2")), 2).alias("avg_loyer_house"));


// APPARTEMENT
        Dataset<Row> dataset_rental_app = filterByDep(dataRentalApp, listDep, "DEP");
        Dataset<Row> dataset_rental_app_cast = dataset_rental_app.withColumn("loypredm2",
                functions.regexp_replace(dataset_rental_app.col("loypredm2"), ",", "."));
        dataset_rental_app.col("loypredm2").cast("float");

        Dataset<Row> dataset_avg_rental_app_commune = dataset_rental_app_cast
                .groupBy(dataset_rental_app_cast.col("LIBGEO").alias("commune"), dataset_rental_app_cast.col("DEP"))
                .agg(round(avg(dataset_rental_app_cast.col("loypredm2")), 2).alias("avg_loyer_app"));

        Dataset<Row> dataset_avg_rental_app_dep = dataset_rental_app_cast
                .groupBy(dataset_rental_app_cast.col("DEP"))
                .agg(round(avg(dataset_rental_app_cast.col("loypredm2")), 2).alias("avg_loyer_app"));

// Jonction des datasets pour un dataset de moyenne locatif par departement
        Dataset<Row> datasetRentalsJoinDep = dataset_avg_rental_house_dep.join(
                dataset_avg_rental_app_dep, dataset_avg_rental_house_dep.col("DEP")
                        .equalTo(dataset_avg_rental_app_dep.col("DEP")), "inner");
        Dataset<Row> datasetRentalsByDep = datasetRentalsJoinDep.select(
                dataset_avg_rental_house_dep.col("*"),
                dataset_avg_rental_app_dep.col("avg_loyer_app"));
//        datasetRentalsFinalByDep.show();

// Jonction des datasets pour un dataset de moyenne locatif par communes
        Dataset<Row> datasetRentalsJoinCommune = dataset_avg_rental_house_commune.join(
                dataset_avg_rental_app_commune, dataset_avg_rental_house_commune.col("commune")
                        .equalTo(dataset_avg_rental_app_commune.col("commune")), "inner");
        Dataset<Row> datasetRentalsByCommune = datasetRentalsJoinCommune.select(
                dataset_avg_rental_house_commune.col("*"),
                dataset_avg_rental_app_commune.col("avg_loyer_app"));

        datasetRentalsByCommune.show();
        Dataset<Row> datasetRentalsFinalByCommune = datasetRentalsByCommune.withColumnRenamed("DEP", "code_departement");
        Dataset<Row> datasetRentalsFinalByDep = datasetRentalsByDep.withColumnRenamed("DEP", "code_departement");

        database.saveDataset("avg_rental_commune", datasetRentalsFinalByCommune);
        database.saveDataset("avg_rental_dep", datasetRentalsFinalByDep);

// FOYERS FISCAUX
        Dataset<Row> dataset_households_with_col_dep =
                dataHouseHolds.withColumn("DEPT", functions.expr("substring(CODGEO, 1, 2)"));
        Dataset<Row> dataset_households_byDep = filterByDep(dataset_households_with_col_dep, listDep, "DEPT");

        dataset_rental_app.show();
        dataset_households_byDep.show();
        Dataset<Row> dataset_households_byDep_clean = dataset_households_byDep
                .filter(
                        dataset_households_byDep.col("Q221").isNotNull())
                .join(dataset_rental_app, dataset_households_byDep.col("CODGEO")
                        .equalTo(dataset_rental_app.col("INSEE_C")), "left");
        Dataset<Row> dataset_households_byDep_filtred = dataset_households_byDep_clean
                .select(
                dataset_households_byDep_clean.col("CODGEO").alias("code_commune"),
                dataset_households_byDep_clean.col("LIBGEO").alias("nom_commune"),
                dataset_households_byDep_clean.col("DEP").alias("code_departement"),
                dataset_households_byDep_clean.col("NBMEN21").alias("Nb_foyers_fiscaux"),
                dataset_households_byDep_clean.col("NBPERS21").alias("Nb_personnes/foyers_fiscaux"),
                dataset_households_byDep_clean.col("Q221").alias("revenus_fiscaux"));

        database.saveDataset("households", dataset_households_byDep_filtred);

// Taux foyers vacants
        Dataset<Row> dataVacant_byDep = filterByDep(dataVacant, listDep, "CODE_DEPT");
        Dataset<Row> dataVacant_byDep_filtred = dataVacant_byDep
                .filter(dataVacant_byDep.col("Prop_logvac_pp_2A_010121").isNotNull()
                        .and(dataVacant_byDep.col("Prop_logvac_pp_C_010121").isNotNull()))
                        .select(
                                dataVacant_byDep.col("NOM_COM").alias("nom_commune"),
                                dataVacant_byDep.col("CODE_DEPT").alias("code_departement"),
                                dataVacant_byDep.col("Prop_logvac_pp_C_010121").alias("taux_vancance_inf2A"),
                                dataVacant_byDep.col("Prop_logvac_pp_2A_010121").alias("taux_vancance_sup2A"));
        database.saveDataset("taux_vacance", dataVacant_byDep_filtred);

// Taux criminalité
        Dataset<Row> dataset_crim_with_col_dep =
                dataCriminalite.withColumn("DEPT", functions.expr("substring(CODGEO_2023, 1, 2)"));
        Dataset<Row> dataset_crim_byDep = filterByDep(dataset_crim_with_col_dep, listDep, "DEPT");
        dataset_crim_byDep.show();
        Dataset<Row> dataset_crimi_byDep_clean = dataset_crim_byDep
                .filter(
                        dataset_crim_byDep.col("faits").isNotNull())
                .join(dataset_rental_app, dataset_crim_byDep.col("CODGEO_2023")
                        .equalTo(dataset_rental_app.col("INSEE_C")), "left");
        Dataset<Row> dataset_crimi_byDep_clean_rename = dataset_crimi_byDep_clean.withColumnRenamed("LIBGEO", "nom_commune");

        Dataset<Row> dataset_crimi_final = dataset_crimi_byDep_clean_rename
                .select(
                        dataset_crimi_byDep_clean_rename.col("nom_commune"),
                        dataset_crimi_byDep_clean_rename.col("annee"),
                        dataset_crimi_byDep_clean_rename.col("faits"),
                        dataset_crimi_byDep_clean_rename.col("POP"))
                .groupBy(dataset_crimi_byDep_clean_rename.col("nom_commune"),
                        dataset_crimi_byDep_clean_rename.col("annee"))
                .agg(avg(dataset_crimi_byDep_clean_rename.col("faits")).alias("nb_faits"),
                        avg(dataset_crimi_byDep_clean_rename.col("POP")).alias("population"));
        database.saveDataset("criminalite", dataset_crimi_final);
//        dataset_crimi_final.show();
    }
}